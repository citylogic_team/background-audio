//-----------------------------Imports

var Observable = require("FuseJS/Observable");
var Storage = require("FuseJS/Storage");

var Time = require("Modules/Time");


//-----------------------------Variable creation
var mustUpdateTime = false
var currentBookmark

var AudioPlaying = Observable(false);
var ChapeterPlayingUrl = "";
var AudioUrlToBePlayed = "";

var myInterval = null;
var PlayInterval = null;
var PauseInterval = null;

var prevDuration = 0;

var PlayAudioAction = Observable(false);
var PauseAudioAction = Observable(false);


var isUserLoggedIn = Observable(true);


var book = this.Parameter;

var title = "A Year in the Wild sample"
var author = "James Hendry"
var narrator = "James Hendry"
var imageUrl = "http://audioshelf.co.za/wp-content/uploads/2017/01/Audible_AYITW-600x600.jpg"
var sampleUrl;
var sampleTime;


var awsSlug;
var slug;


var chapters;
var CurrentChapter = Observable();

var sliderProgress = Observable(0);
var sliderProgressText = Observable(function() { return Math.round(56 * sliderProgress.value); });

var timePassed = Observable( function() { return Time.TimeFormat(sliderProgressText.value) } );


var secondsOnly = Observable(0);

var chapter = Observable();
var TrackUrl = Observable();
var TrackTime = Observable();

var PlayingChapter = Observable();
var AutoPlayVideo = Observable(false);
var SaveBoodDetails = [];
var IsLastBook = Observable(false);


var goToChapterState = true;

//-----------------------------Track Selection

function PlayAudioLoop(){
	
}

//-----------------------------Timeline Manipulation

function updateSliderTime(){
	prevDuration = sliderProgress.value;
}




// When the play button gets pressed
function PlayButton(){
	PlayAudioLoop()

	AudioPlaying.value = true;
}

// When the pause button gets pressed
function PauseButton(){


	AudioPlaying.value = false;

}


// When the back button gets pressed
var backFewSeconds = function(){

	var fullTimeSeconds = Math.round(secondsOnly.value);
	if( (sliderProgress.value - (15 / fullTimeSeconds)) <= 0){
		sliderProgress.value = 0;
	}else{
		sliderProgress.value = sliderProgress.value - (15 / fullTimeSeconds);
	}
}

// When the forward button gets pressed
var forwardFewSeconds = function(){

	var fullTimeSeconds = Math.round(secondsOnly.value);

	if( (sliderProgress.value + (15 / fullTimeSeconds)) >= 1){
		sliderProgress.value = 1;

		NextChapter()
	}else{
		sliderProgress.value = sliderProgress.value + (15 / fullTimeSeconds);
	}

}


//-----------------------------All exports


// The loading animations
function startLoading() {
	
}

module.exports = {
	sliderProgress,
	PlayButton,
	PauseButton,
	backFewSeconds,
	forwardFewSeconds,
	AudioPlaying,
	PlayAudioAction,
	updateSliderTime,
	timePassed,
	AutoPlayVideo
};	



