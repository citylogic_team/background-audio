var Observable = require("FuseJS/Observable");

function TimeFormat(ms) {

	var fullTimeSeconds = ms;

	var hours = (Math.floor(fullTimeSeconds / 3600)).toString()
	var minutes = (Math.floor((fullTimeSeconds - (hours * 3600))/ 60)).toString()
	var seconds = (fullTimeSeconds - (minutes * 60) - (hours * 3600)).toString()

	if(seconds <= 9 ){ seconds = '0' + seconds;	} 
	if(minutes <= 9 ) { minutes = '0' + minutes; }
	
	var timeString = "";

	if( hours != 0 ){

		timeString = hours + ":" + minutes + ":" + seconds;
	} else {

		timeString = minutes + ":" + seconds;
	}

	return timeString
}

function chapterFormat(chapterTime) {

	return TimeFormat(secondsOnly(chapterTime))
}

function secondsOnly(RecivedTime) {

	var a = RecivedTime.split(':');
	var seconds = (+a[0]) * 60 + (+a[1])

	return seconds;
}



module.exports = {
	TimeFormat,
	chapterFormat,
	secondsOnly
};